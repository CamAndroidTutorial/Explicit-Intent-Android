package com.bunhann.explicitintentandroid;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

public class SecondActivity extends AppCompatActivity {

    private EditText edName, edPhone;
    private String name, phone;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_second);

        edName = findViewById(R.id.edName);
        edPhone = findViewById(R.id.edPhone);

        Bundle extras = getIntent().getExtras();
        if (extras!=null){
            name = extras.getString("MYNAME");
            phone = extras.getString("PHONE");

            edName.setText(name);
            edPhone.setText(phone);
        }

    }

}
